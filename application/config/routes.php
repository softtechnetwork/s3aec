<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';


/* ================= API ===================== */
$route['(:any)/api/SetLoginWithFacebook'] = "S3Module/SetLoginWithFacebook";
$route['(:any)/api/SetLoginWithLine'] = "S3Module/SetLoginWithLine";
$route['(:any)/api/SetLoginWithGoogle'] = "S3Module/SetLoginWithGoogle";
$route['(:any)/api/GetAllChannelList'] = "S3Module/GetAllChannelList";
$route['(:any)/api/SetChannelClick'] = "S3Module/SetChannelClick";
$route['(:any)/api/GetChannelRecentView'] = "S3Module/GetChannelRecentView";
$route['(:any)/api/SetLoginWithGuest'] = "S3Module/SetLoginWithGuest";
$route['(:any)/api/SetChipCodeLatLon'] = "S3Module/SetChipCodeLatLon";
$route['(:any)/api/GetYoutubeCategory'] = "S3Module/GetYoutubeCategory";
$route['(:any)/api/SetMemberGenderAndBirthday'] = "S3Module/SetMemberGenderAndBirthday";
$route['(:any)/api/SetMemberDeviceToken'] = "S3Module/SetMemberDeviceToken";
//$route['api/SetYoutubeAPIKeyOverLimit'] = "S3Module/SetYoutubeAPIKeyOverLimit";

$route['(:any)/api/SetYoutubeViewLogs'] = "S3Module/SetYoutubeViewLogs";

# INTERNET TV Channel Click
/* check all internet tv channel list  (s3 remote service) */
$route['(:any)/api/GetInternetTvChannelList'] = "S3Module/GetInternetTvChannelList" ;
$route['(:any)/api/SetInternetTvChannelClick'] = "S3Module/SetInternetTvChannelClick";
#EOF INTERNET TV Channel Click


/* additional 25.10.18 */
$route['(:any)/api/GetChannelListByBandType'] = "S3Module/GetChannelListByBandType";

/* api for rating box */
$route['(:any)/api/SetChannelRating'] =  "S3Module/SetChannelRating";

/* channel advertisement popup */
$route['(:any)/api/GetChannelAdvertisementMobilePopup'] = "S3Module/GetChannelAdvertisementMobilePopup"; 


$route['(:any)/welcome/getTotalInternetTVOnline'] = "welcome/getTotalInternetTVOnline";
//$route['backend'] = "Welcome/tv";

/* get history link log (s3 remote service) */
$route['(:any)/api/GetHistoryLinkLogs'] = "S3Module/GetHistoryLinkLogs";

/* set history link log (s3 remote service) */
$route['(:any)/api/SetHistoryLinkLogs'] = "S3Module/SetHistoryLinkLogs";

/* edit history link log (s3 remote service) */
$route['(:any)/api/EditHistoryLinkLogs'] = "S3Module/EditHistoryLinkLogs";

/* delete history link log (s3 remote service) */
$route['(:any)/api/DelHistoryLinkLogs'] = "S3Module/DelHistoryLinkLogs";

/* check stream channel (s3 remote service) */
$route['(:any)/api/CheckStreamChannel'] = "S3Module/CheckStreamChannel" ;

/* check stream channel (s3 remote service) */
$route['(:any)/api/CheckDownloadLink'] = "S3Module/CheckDownloadLink" ;

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
